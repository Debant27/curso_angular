// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-coverage-istanbul-reporter'),
      require('@angular-devkit/build-angular/plugins/karma')
    ],
    client: {
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    coverageIstanbulReporter: {
      dir: require('path').join(__dirname, './coverage/angular-whislist'),
      reports: ['html', 'lcovonly', 'text-summary'],
      fixWebpackSourcePaths: true
    },
    reporters: ['progress', 'kjhtml'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autowatch: false,  //**Se debe poner asi para circleci**
    //autoWatch: true,  
    browsers: ['Chrome', 'ChromeHeadless', 'ChromeHeadlessCI'],
    //**Se debe poner asi para circleci**
    customLauchers: {
      ChromeHeadlessCI: { //Significa que es un modo de Chrome no interactivo
        base: 'ChromeHeadless',
          flags: ['--no-sandbox','--disable-gpu','--disable-translate','--disable-extensions','--remote-debbuging-port=9223']
      }
    },
    singleRun: false,
    restartOnFileChange: true
  });
};
