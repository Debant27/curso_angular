import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destinos-viajes-state.model';
import { DestinosApiClient } from "./../../models/destinos-api-client.model";

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  all;
  //destinos: DestinoViaje[];

  constructor(public destinosApiClient: DestinosApiClient, private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.destinos.favorito)
      .subscribe(d => {
        if (d != null){
          this.updates.push('Se ha elegido a '+ d.nombre);
        }
      });
      store.select(state => state.destinos.items).subscribe(items => this.all = items);
   }

  ngOnInit() {
  }

  agregado(d: DestinoViaje) {
    //let d = new DestinoViaje(nombre, url);
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
    //this.store.dispatch(new NuevoDestinoAction(d));
    //this.destinos.push(new DestinoViaje(nombre, url));
    //return false;
  }

  elegido(e: DestinoViaje){
    //this.destinos.forEach(function (x) {x.setSelected(false); });
    //e.setSelected(true);
    this.destinosApiClient.elegir(e);
    //this.store.dispatch(new ElegidoFavoritoAction(e));
  }

  getAll() {
     
  }
}

