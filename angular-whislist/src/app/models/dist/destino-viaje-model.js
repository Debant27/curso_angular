"use strict";
exports.__esModule = true;
exports.DestinoViaje = void 0;
var DestinoViaje = /** @class */ (function () {
    function DestinoViaje(nombre, u) {
        this.nombre = nombre;
        this.u = u;
    }
    DestinoViaje.prototype.isSelected = function () {
        return this.selected;
    };
    DestinoViaje.prototype.setSelected = function () {
        this.selected = true;
    };
    return DestinoViaje;
}());
exports.DestinoViaje = DestinoViaje;
