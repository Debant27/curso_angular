"use strict";
exports.__esModule = true;
exports.DestinosApiClient = void 0;
var rxjs_1 = require("rxjs");
var DestinosApiClient = /** @class */ (function () {
    function DestinosApiClient() {
        this.current = new rxjs_1.BehaviorSubject(null);
        this.destinos = [];
    }
    DestinosApiClient.prototype.add = function (d) {
        this.destinos.push(d);
    };
    DestinosApiClient.prototype.getAll = function () {
        return this.destinos;
    };
    DestinosApiClient.prototype.getById = function (id) {
        return this.destinos.filter(function (d) { return d.id.toString() === id; })[0];
    };
    DestinosApiClient.prototype.elegir = function (d) {
        this.destinos.forEach(function (x) { return x.setSelected(false); });
        d.setSelected(true);
        this.current.next(d);
    };
    DestinosApiClient.prototype.subscribeOnChange = function (fn, _a) {
        var  = _a["this"], current = _a.current, subscribe = _a.subscribe;
    };
    return DestinosApiClient;
}());
exports.DestinosApiClient = DestinosApiClient;
(fn);
