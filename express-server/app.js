var express = require("express"), cors = require('cors'); //requiriendo el modulo express
var app = express(); //inicializando un servidor web
app.use(express.json()); //vamos a usar json
app.use(cors());
app.listen(3000, () => console.log("Server running on port 3000")); //va estar en el puerto 3000 cuando inicie va llamar al callback y nos muestra el mensaje en la consola

var ciudades = [ "Paris", "Barcelona", "Barranquilla", "Montevideo", "Santiago de Chile",
"Mexico DF","Nueva York"];
app.get("/ciudades", (req, res, next) => res.json(ciudades.filter((c)=> c.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1))); //cuando vayamos al root/url nos va retornar el res.json con un array

var misDestinos= []; //
app.get("/my", (req, res, next) => res.json(misDestinos)); //es para mis destinos, get me da mis destinos
app.post("/my", (req, res, next) => {
    console.log(req.body);
    misDestinos.push(req.body.nuevo);
    res.json(misDestinos); //post de my le pasamos todos los destinos.
});
//El servidor se corre con npm app.js

app.get("/api/translation", (req, res, next) => res.json([
    {lang: req.query.lang, key: 'HOLA', value: 'HOLA ' + req.query.lang}
]));